Pod::Spec.new do |s|

  s.authors      = "Kerem",
  s.name         = "OnfidoTask"
  s.summary      = "OnfidoTask SDK"
  s.version      = "0.3"
  s.homepage     = "http://www.google.com"
  s.license      = { :type => 'Apache 2.0', :file => 'LICENSE' }

  s.platform     = :ios, "11.0"
  s.source       = { :git => "https://kerem1905@bitbucket.org/kerem1905/onfido-release.git", :tag => "release/v#{s.version}"}

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #

  s.source_files        = "Framework/OnfidoTask.framework/Headers/*.h"
  s.public_header_files = "Framework/OnfidoTask.framework/Headers/*.h"
  s.vendored_frameworks = "Framework/OnfidoTask.framework"
  s.frameworks          = "UIKit", 'Foundation'


end